#!/usr/bin/env python
# encoding: utf-8

import tweepy #https://github.com/tweepy/tweepy
import csv

#Twitter API credentials
consumer_key = "U65bTXrEEtoi4cXE3BbO5EohU"
consumer_secret = "PnBxisnT2fnnDrmu6HGFJflMtPzlAZLLFKpxGySS2350pf3Mfw"
access_key = "448986414-Iy9HzUxVCF4I0YIkIDBAUOZutdadZp3uJ2Cu7MJF"
access_secret = "rpY09yf1cZKUD0wh6c6eM8TkvaT3oefgjQE0hIyqPjxAN"

# 	Consumer Key (API Key)	U65bTXrEEtoi4cXE3BbO5EohU
# Consumer Secret (API Secret)	PnBxisnT2fnnDrmu6HGFJflMtPzlAZLLFKpxGySS2350pf3Mfw

# 	Access Token	448986414-Iy9HzUxVCF4I0YIkIDBAUOZutdadZp3uJ2Cu7MJF
# Access Token Secret	rpY09yf1cZKUD0wh6c6eM8TkvaT3oefgjQE0hIyqPjxAN

def get_all_tweets(screen_name):
	#Twitter only allows access to a users most recent 3240 tweets with this method
	
	#authorize twitter, initialize tweepy
	auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
	auth.set_access_token(access_key, access_secret)
	api = tweepy.API(auth)
	
	#initialize a list to hold all the tweepy Tweets
	alltweets = []	
	
	#make initial request for most recent tweets (200 is the maximum allowed count)
	new_tweets = api.user_timeline(screen_name = screen_name,count=200)
		
	print(new_tweets[0])

	#save most recent tweets
	alltweets.extend(new_tweets)
	
	#save the id of the oldest tweet less one
	oldest = alltweets[-1].id - 1
	
	#keep grabbing tweets until there are no tweets left to grab
	while len(new_tweets) > 0:
		print "getting tweets before %s" % (oldest)
		
		#all subsiquent requests use the max_id param to prevent duplicates
		new_tweets = api.user_timeline(screen_name = screen_name, count = 200, max_id = oldest)
		
		#save most recent tweets
		alltweets.extend(new_tweets)
		
		#update the id of the oldest tweet less one
		oldest = alltweets[-1].id - 1
		
		print "...%s tweets downloaded so far" % (len(alltweets))
	
	#transform the tweepy tweets into a 2D array that will populate the csv	
	print(str(alltweets[0]))
	outtweets = [[tweet.id_str, tweet.created_at, tweet.text.encode("utf-8").replace('\n',''), tweet.is_quote_status, tweet.source] for tweet in alltweets]
	
	#write the csv	
	with open('%s_tweets.tsv' % screen_name, 'wb') as f:
		[f.write("{}".format(col)) for col in ["id\t","created_at\t","text\t","is_quote_status\t","source\n"]]

		for row in outtweets:
			[f.write("{}".format(col)) for col in [str(row[0])+"\t",str(row[1])+"\t",str(row[2])+"\t",str(row[3])+"\t",str(row[4])+"\n"]]

		# writer = csv.writer(f)
		# writer.writerow(["id","created_at","text","source"])
		# writer.writerows(outtweets)
	
	pass


if __name__ == '__main__':
	#pass in the username of the account you want to download
	get_all_tweets("realDonaldTrump")