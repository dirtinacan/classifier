import re

stopwords = [word.replace('\n','') for word in open('stopwords.txt','r').readlines()]

def find_urls(text):
	return re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', text)

def find_hashtags(text):
	return re.findall(r"#(\w+)", text)

def find_mentions(text):
	return re.findall(r"@(\w+)", text)

def process_data(filename='realDonaldTrump_tweets_android.tsv'):
	result = {
		"number_urls": [],
		"percentage_caps": [],
		"number_hashtags": [],
		"number_mentions": [],
		"most_used_words": {
			"iphone": [],
			"android": []
		}
	}
	most_used_words = {
		"iphone": {},
		"android": {}
	}
	file = open(filename,'r')
	for line in file.readlines():
		line = line.replace('\n','').split('\t')
		
		device = "iphone" if "iPhone" in line[3] else "android"

		f = open('tweets/tweet_{}_{}.txt'.format(line[0],device),'w')

		text = line[2]
		

		f.write(text)
		f.close()




		urls = find_urls(text)



		# replace urls
		number_urls = len(urls)
		for url in urls:
			text = text.replace(url,'')

		# replace hashtags
		hashtags = find_hashtags(text)
		number_hashtags = len(hashtags)
		for hashtag in hashtags:
			text = text.replace('#{}'.format(hashtag),'')

		# replace mentions
		mentions = find_mentions(text)
		number_mentions = len(mentions)
		for mention in mentions:
			text = text.replace('@{}'.format(mention),'')

		text = text.replace('  ','')

		percentage_caps = 0 if not len(text) else sum(1 for c in text if c.isupper()) * 100.0/ len(text)

		text = ''.join([i.lower() for i in text if i.isalpha() or i==' '])

		words = [word for word in text.split(' ')]

		for word in words:
			if word not in most_used_words[device]:
				most_used_words[device][word] = 0
			most_used_words[device][word] += 1

		result['number_urls'].append(number_urls)
		result['percentage_caps'].append(percentage_caps)
		result['number_hashtags'].append(number_hashtags)
		result['number_mentions'].append(number_mentions)

	for device in ['iphone','android']:
		temp = [{'word': key, 'count': value} for key, value in most_used_words[device].iteritems()]
		temp.sort(key=lambda x: x['count'], reverse=True)
		temp = [entry for entry in temp if entry['word'] not in stopwords]
		print(device)
		print(temp[:50])
		print('\n')

if __name__ == '__main__':
	process_data()