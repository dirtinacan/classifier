import nltk.classify.util
from nltk.classify import NaiveBayesClassifier
from nltk.corpus import movie_reviews
from nltk.corpus import stopwords
from process_data import find_urls, find_hashtags, find_mentions

import string
 
punctuation = list(string.punctuation)
stop = stopwords.words('english') + punctuation + ['rt', 'via']

from nltk.corpus.reader import CategorizedPlaintextCorpusReader

reader = CategorizedPlaintextCorpusReader('./tweets/', r'.*\.txt', cat_pattern=r'tweet_\d+_(\w+).txt')

NUMBER_FEATURES = 2000

print "Naive Bays with the {0} most frequent words as features".format(NUMBER_FEATURES)


def takeout_stopwords(words):
	print(words)
	return [word for word in words if word not in stop]



documents = [(takeout_stopwords(list(reader.words(fileid))), category)
			for category in reader.categories()
			for fileid in reader.fileids(category)]




def document_features(document):
    document_words = set(document)
    features = {}
    for word in word_features:
        features['contains(%s)' % word] = (word in document_words)
    return features

all_words = nltk.FreqDist(w.lower() for w in reader.words())
word_features = all_words.keys()[:NUMBER_FEATURES]

featuresets = [(document_features(d), c) for (d,c) in documents]
trainVStest = len(featuresets)*3/4
trainfeats = featuresets[:trainVStest]
testfeats = featuresets[trainVStest:]
print 'train on %d instances, test on %d instances' % (len(trainfeats), len(testfeats))

classifier = NaiveBayesClassifier.train(trainfeats)
print 'accuracy:', nltk.classify.util.accuracy(classifier, testfeats)

classifier.show_most_informative_features(30)